# infra-challenge-ak
Basic load balancer to play around with a bunch of nifty techs.

## Requirements
Version requirements just mean that it's what I used to develop the app with  
so take those with a grain of salt.

* Vagrant 2.2.9 (with virtualbox provisioner)
* Ansible 2.9.9
* Docker version 19.03.11-ce 
* Some docker registry (gitlab provides one for free)

## Usage
```bash
# To provision
ansible-galaxy install geerlingguy.docker
ansible-galaxy install geerlingguy.pip
vagrant up

# To ensure that it works as intended
curl localhost:4000/ping # Should receive {"message":"pong"}

# To update
docker login registry.gitlab.com # If first time updating
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory update.yaml
```

## How does it work?
* Vagrant sets up a couple of vms
  * First for haproxy
  * Second for app
* Ansible `provision.yaml` provisions the vms which...
  * Installs docker
  * Installs ansible dependencies to play with docker
  * Starts the ha docker container
  * Starts the app docker containers
* Ansible `update.yaml` updates the containers without disturbing the users by...
  * Building the ha/app images locally
  * Sending those images to the gitlab registry
  * Restarts ha container
  * Restarts app containers one by one

And clients dont see any downtime because ha runs backround checks on each server before the traffic.

